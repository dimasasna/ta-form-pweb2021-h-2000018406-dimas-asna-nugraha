<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Website Portofolio</title>
    <link rel="stylesheet" href="style1.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    
</head>
<body>

    <!--awal Bagian Header dan Nav-->
    <header>
        <h2>Dimas Nugraha</h2>
        <nav>
            <ul class="nav-links">
                <li><a href="#about">About</a></li>
                <li><a href="#form">Form</a></li>
                <li><a href="#info">Info</a></li>
            </ul>
        </nav>
        <a class="cta" href="#media-social"><button>Social Media</button></a>
    </header>
    <!--akhir Bagian Header dan Nav-->

    <!--awal Bagian about-->
    <div id="About" class="box">
        <h2>SELAMAT DATANG</h2><br>
        <h4>FORMULIR PENDAFTARAN MAHASISWA BERPRESTASI  </h4><br>
    </div>
    <!--akhir Bagian about-->
    
    <main class="artikel">
        <!--awal Bagian article-->
        <h1>Personal Data</h1>
    <article id="form">
        <form action="hasil.php" method="POST" name="biodata" onsubmit="return validateform()">
            <table>
                <div class="col-3">
                    <input type="text" name="nama" class="effect-1" placeholder="Masukan Nama Anda" required>
                    <span class="focus-border"></span>
                </div>
                <div class="col-3">
                    <input type="text" name="angkatan" class="effect-1" placeholder="Masukan Angkatan Anda" required>
                    <span class="focus-border"></span>
                </div>
                <div class="col-3">
                    <input type="text" name="nim" class="effect-1" placeholder="Masukan Nomor Induk Mahasiswa" required>
                    <span class="focus-border"></span>
                </div>
                <div class="col-3">
                    <input type="text" name="telp" class="effect-1" placeholder="Masukan Nomor Telepon Anda" required>
                    <span class="focus-border"></span>
                </div>
                <div class="col-3">
                    <input type="text" name="prodi" class="effect-1" placeholder="Masukan Program Studi" required>
                    <span class="focus-border"></span>
                </div>
                <div class="radio-group">
                <label class="radio">
                    <input type="radio" value="Laki-Laki" name="Gender" required>Laki Laki
                    <span></span>
                </label>
                <label class="radio">
                    <input type="radio" value="Perempuan" name="Gender" required>Perempuan
                    <span></span>
                </label>
                </div>
                <button type="submit" name="submit" value="SEND">Kirim</button>
                <button class="button10" type="reset">Reset</button>
            </table>
        </form>  
    </article>
    </main>
    <!--Akhir bagian article-->

    <!--Awal bagian aside-->
        <aside>
        <div class="aside">
        <h3 id="info">Mahasiswa Berprestasi</h3>
        <p>Mahasiswa Berprestasi adalah mahasiswa yang berhasil mencapai prestasi tinggi, baik akademik maupun non akademik, mampu berkomunikasi dengan bahasa Indonesia dan bahasa Inggris, bersikap positif, serta berjiwa Pancasila.Pemilihan Mawapres ini akan terus ditingkatkan kualitasnya dalam rangka memberikan motivasi berprestasi di kalangan mahasiswa dan menciptakan budaya akademik yang baik. Selain itu, diharapkan proses pemilihan ini dapat diadopsi menjadi sebuah system pembinaan prestasi diperguruan tinggi.</p><br><br>
        <h2 id="media-social">Social Media</h2>
        <p>Click Me</p><br>
        <a style="color: black;" href="https://www.facebook.com/mas.dimkas.1/"><i class="fa fa-facebook fa-3x" aria-hidden="true"></i></a>&ensp;
        <a style="color: black;"href="https://twitter.com/dmsasna"><i class="fa fa-twitter fa-3x" aria-hidden="true"></i></a>&ensp;
        <a style="color: black;"href="https://www.instagram.com/dimasasna/"><i class="fa fa-instagram fa-3x" aria-hidden="true"></i></a>&ensp;
        <a style="color: black;"href="https://api.whatsapp.com/send?%20phone=6287736854680"><i class="fa fa-whatsapp fa-3x" aria-hidden="true"></i></a>&ensp;
        </div>
        </aside>

    
    <!--Akhir bagian aside-->
    
    
    <!--awal footer-->
    <footer>
        <p>Created with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="">Dimas Asna Nugraha</a></p>
    </footer>
    <!--akhir footer-->

    
</body>
</html>